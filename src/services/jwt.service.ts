import {/* inject, */ BindingScope, inject, injectable} from '@loopback/core';
import {repository} from '@loopback/repository';
import {HttpErrors} from '@loopback/rest';
import {promisify} from 'util';
import {TokenServiceBindings} from '../keys';
import {UserRepository} from '../repositories';

const jwt = require('jsonwebtoken');
const jwtSign = promisify(jwt.sign);
const jwtVerify = promisify(jwt.verify);

@injectable({scope: BindingScope.TRANSIENT})
export class JwtService {
  constructor(
    @inject(TokenServiceBindings.TOKEN_SECRET)
    public tokenSecret: string,
    @inject(TokenServiceBindings.TOKEN_EXPIRE)
    public tokenExpire: string,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) {}

  async generateToken(userInfo: any): Promise<any> {
    let token: string;

    try {
      token = await jwtSign(userInfo, this.tokenSecret, {expiresIn: '1h'});
    } catch (err) {
      throw new HttpErrors.BadRequest(`Error Encoding Token: ${err}`);
    }

    return token;
  }

  async verifyToken(token: string): Promise<any> {
    if (!token) {
      throw new HttpErrors.Unauthorized(`Error verifying token: token is null`);
    }

    let decodeToken;

    try {
      decodeToken = await jwtVerify(token, this.tokenSecret);
    } catch (err) {
      throw new HttpErrors.BadRequest(err);
    }

    let getUserWithToken = await this.userRepository.findOne({
      where: {id: decodeToken.id},
      include: [
        {
          relation: 'tokens',
          scope: {
            where: {
              user_id: decodeToken.id,
              token: token,
            },
          },
        },
      ],
    });

    if (!getUserWithToken?.tokens) {
      throw new HttpErrors.Unauthorized(`Error verifying token`);
    }

    let TokenA = getUserWithToken.tokens[0];

    let user: any = Object.assign({
      email: decodeToken.email,
      id: decodeToken.id,
      token: token,
      TokenA,
    });

    return user;
  }
}
