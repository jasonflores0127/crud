import {Entity, hasMany, model, property} from '@loopback/repository';
import {Token} from './token.model';
import {UserOrder} from './user-order.model';

@model()
export class User extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    index: {
      unique: true,
    },
  })
  email?: string;

  @property({
    type: 'string',
  })
  password?: string;

  @property({
    type: 'number',
  })
  attemp: number;

  @hasMany(() => Token, {keyTo: 'user_id'})
  tokens: Token[];

  @hasMany(() => UserOrder, {keyTo: 'user_id'})
  userOrders: UserOrder[];

  constructor(data?: Partial<User>) {
    super(data);
  }
}

export interface UserRelations {
  // describe navigational properties here
}

export type UserWithRelations = User & UserRelations;
