import {Entity, model, property} from '@loopback/repository';

@model()
export class UserOrder extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
  })
  user_id?: number;

  @property({
    type: 'number',
  })
  product_id?: number;

  @property({
    type: 'number',
  })
  quantity?: number;

  constructor(data?: Partial<UserOrder>) {
    super(data);
  }
}

export interface UserOrderRelations {
  // describe navigational properties here
}

export type UserOrderWithRelations = UserOrder & UserOrderRelations;
