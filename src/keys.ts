import {BindingKey} from '@loopback/context';
import {JwtService} from './services/jwt.service';

export namespace TokenServiceInstant {
  export const TOKEN_SECRET_VALUE = 'secret';
  export const TOKEN_EXPIRE_VALUE = '600';
}

export namespace TokenServiceBindings {
  export const TOKEN_SECRET = BindingKey.create<string>(
    'authentication.jwt.secret',
  );
  export const TOKEN_EXPIRE = BindingKey.create<string>(
    'authentication.jwt.expire',
  );
  export const TOKEN_SERVICE = BindingKey.create<JwtService>(
    'authentication.jwt.jwtservice',
  );
}
