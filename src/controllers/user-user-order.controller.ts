import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  User,
  UserOrder,
} from '../models';
import {UserRepository} from '../repositories';

export class UserUserOrderController {
  constructor(
    @repository(UserRepository) protected userRepository: UserRepository,
  ) { }

  @get('/users/{id}/user-orders', {
    responses: {
      '200': {
        description: 'Array of User has many UserOrder',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(UserOrder)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<UserOrder>,
  ): Promise<UserOrder[]> {
    return this.userRepository.userOrders(id).find(filter);
  }

  @post('/users/{id}/user-orders', {
    responses: {
      '200': {
        description: 'User model instance',
        content: {'application/json': {schema: getModelSchemaRef(UserOrder)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof User.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UserOrder, {
            title: 'NewUserOrderInUser',
            exclude: ['id'],
            optional: ['user_id']
          }),
        },
      },
    }) userOrder: Omit<UserOrder, 'id'>,
  ): Promise<UserOrder> {
    return this.userRepository.userOrders(id).create(userOrder);
  }

  @patch('/users/{id}/user-orders', {
    responses: {
      '200': {
        description: 'User.UserOrder PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UserOrder, {partial: true}),
        },
      },
    })
    userOrder: Partial<UserOrder>,
    @param.query.object('where', getWhereSchemaFor(UserOrder)) where?: Where<UserOrder>,
  ): Promise<Count> {
    return this.userRepository.userOrders(id).patch(userOrder, where);
  }

  @del('/users/{id}/user-orders', {
    responses: {
      '200': {
        description: 'User.UserOrder DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(UserOrder)) where?: Where<UserOrder>,
  ): Promise<Count> {
    return this.userRepository.userOrders(id).delete(where);
  }
}
