import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  HttpErrors,
  param,
  patch,
  post,
  put,
  requestBody,
  response,
} from '@loopback/rest';
import {SecurityBindings} from '@loopback/security';
import {TokenServiceBindings} from '../keys';
import {User} from '../models';
import {
  ProductRepository,
  UserOrderRepository,
  UserRepository,
} from '../repositories';
import {JwtService} from '../services/jwt.service';

export class UserController {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(ProductRepository)
    public productRepository: ProductRepository,
    @repository(UserOrderRepository)
    public userOrderRepository: UserOrderRepository,
    @inject(TokenServiceBindings.TOKEN_SERVICE)
    public tokenService: JwtService,
  ) {}

  @post('/register')
  @response(201, {
    description: 'User model instance',
    content: {'application/json': {schema: getModelSchemaRef(User)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {
            title: 'NewUser',
            exclude: ['id'],
          }),
        },
      },
    })
    user: Omit<User, 'id'>,
  ): Promise<any> {
    let isTaken = await this.userRepository.findOne({where: {email: user.email}});

    if (isTaken) {
      throw new HttpErrors.BadRequest('Email is already Taken');
    }

    const us = await this.userRepository.create(user);

    return {
      message: 'User Successfuly Registered',
      us,
    };
  }

  @get('/users/count')
  @response(200, {
    description: 'User model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(@param.where(User) where?: Where<User>): Promise<Count> {
    return this.userRepository.count(where);
  }

  @get('/users')
  @response(200, {
    description: 'Array of User model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(User, {includeRelations: true}),
        },
      },
    },
  })
  async find(@param.filter(User) filter?: Filter<User>): Promise<User[]> {
    return this.userRepository.find(filter);
  }

  @patch('/users')
  @response(200, {
    description: 'User PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {partial: true}),
        },
      },
    })
    user: User,
    @param.where(User) where?: Where<User>,
  ): Promise<Count> {
    return this.userRepository.updateAll(user, where);
  }

  @get('/users/{id}')
  @response(200, {
    description: 'User model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(User, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(User, {exclude: 'where'}) filter?: FilterExcludingWhere<User>,
  ): Promise<User> {
    return this.userRepository.findById(id, filter);
  }

  @patch('/users/{id}')
  @response(204, {
    description: 'User PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {partial: true}),
        },
      },
    })
    user: User,
  ): Promise<void> {
    await this.userRepository.updateById(id, user);
  }

  @put('/users/{id}')
  @response(204, {
    description: 'User PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() user: User,
  ): Promise<void> {
    await this.userRepository.replaceById(id, user);
  }

  @del('/users/{id}')
  @response(204, {
    description: 'User DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.userRepository.deleteById(id);
  }

  @authenticate('jwt')
  @get('/who')
  async who(
    @inject(SecurityBindings.USER)
    userA: any,
  ): Promise<any> {
    return userA;
  }

  @post('/login')
  async login(@requestBody() userI: any): Promise<any> {
    let user = await this.userRepository.findOne({
      where: {email: userI.email},
    });

    if (!user) throw new HttpErrors.Unauthorized('Invalid Credential');

    // Check if password isMatch

    let isMatch = user.password === userI.password;

    if (!isMatch) {
      if (user.attemp === 5) {
        throw new HttpErrors.Unauthorized(
          'Maximum Failed Attemp try after 5mins',
        );
      }
      await this.userRepository.updateById(user.id, {attemp: user.attemp + 1});
      throw new HttpErrors.Unauthorized('Invalid Credential');
    }

    const userInfoForToken: any = {
      email: user.email,
      id: <number>user.id,
    };

    const token = await this.tokenService.generateToken(userInfoForToken);

    await this.userRepository.tokens(user.id).create({
      token: token,
      user_id: user.id,
    });

    return {
      access_token: token,
    };
  }

  @authenticate('jwt')
  @post('/order')
  async order(
    @requestBody() userReq: any,
    @inject(SecurityBindings.USER)
    userA: any,
  ): Promise<any> {
    let product = await this.productRepository.findOne({
      where: {id: userReq.id},
    });

    if (!product) {
      throw new HttpErrors.BadRequest('Product not found');
    }

    if (product.stock < userReq.quantity) {
      throw new HttpErrors.BadRequest(
        'Failed to order this product due to unavailability of the stock',
      );
    }

    let isInUserOrder = await this.userOrderRepository.findOne({
      where: {
        user_id: userA.id,
        product_id: product.id,
      },
    });

    if (isInUserOrder) {
      await this.userOrderRepository.updateById(isInUserOrder.id, {
        quantity: isInUserOrder.quantity + userReq.quantity,
      });
    } else {
      await this.userOrderRepository.create({
        user_id: userA.id,
        product_id: product.id,
        quantity: userReq.quantity,
      });
    }

    let newStock = product.stock - userReq.quantity;

    await this.productRepository.updateById(product.id, {
      stock: newStock,
    });

    return {
      message: 'You have successfully ordered this product.',
    };
  }
}
