import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {CrudDataSource} from '../datasources';
import {UserOrder, UserOrderRelations} from '../models';

export class UserOrderRepository extends DefaultCrudRepository<
  UserOrder,
  typeof UserOrder.prototype.id,
  UserOrderRelations
> {
  constructor(
    @inject('datasources.crud') dataSource: CrudDataSource,
  ) {
    super(UserOrder, dataSource);
  }
}
