import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {CrudDataSource} from '../datasources';
import {User, UserRelations, Token, UserOrder} from '../models';
import {TokenRepository} from './token.repository';
import {UserOrderRepository} from './user-order.repository';

export class UserRepository extends DefaultCrudRepository<
  User,
  typeof User.prototype.id,
  UserRelations
> {

  public readonly tokens: HasManyRepositoryFactory<Token, typeof User.prototype.id>;

  public readonly userOrders: HasManyRepositoryFactory<UserOrder, typeof User.prototype.id>;

  constructor(
    @inject('datasources.crud') dataSource: CrudDataSource, @repository.getter('TokenRepository') protected tokenRepositoryGetter: Getter<TokenRepository>, @repository.getter('UserOrderRepository') protected userOrderRepositoryGetter: Getter<UserOrderRepository>,
  ) {
    super(User, dataSource);
    this.userOrders = this.createHasManyRepositoryFactoryFor('userOrders', userOrderRepositoryGetter,);
    this.registerInclusionResolver('userOrders', this.userOrders.inclusionResolver);
    this.tokens = this.createHasManyRepositoryFactoryFor('tokens', tokenRepositoryGetter,);
    this.registerInclusionResolver('tokens', this.tokens.inclusionResolver);
  }
}
