import {AuthenticationStrategy} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {HttpErrors, Request} from '@loopback/rest';
import {UserProfile} from '@loopback/security';
import {TokenServiceBindings} from './keys';
import {JwtService} from './services/jwt.service';
export class JWTAuthenticationStrategy implements AuthenticationStrategy {
  name = 'jwt';

  constructor(
    @inject(TokenServiceBindings.TOKEN_SERVICE)
    public tokenServ: JwtService,
  ) {}

  async authenticate(req: Request): Promise<UserProfile | any> {
    const token = this.extractCredentials(req);
    const user = await this.tokenServ.verifyToken(token);
    return user;
  }

  extractCredentials(req: Request): string {
    if (!req.headers.authorization) {
      throw new HttpErrors.Unauthorized(`Authorization header not found.`);
    }
    // for example : Bearer xxx.yyy.zzz
    const authHeaderValue = req.headers.authorization;

    if (!authHeaderValue.startsWith('Bearer')) {
      throw new HttpErrors.BadRequest(
        `Authorization header is not of type 'Bearer'.`,
      );
    }

    //split the string into 2 parts : 'Bearer ' and the `xxx.yyy.zzz`
    const parts = authHeaderValue.split(' ');
    if (parts.length !== 2) {
      throw new HttpErrors.BadRequest(
        `Authorization header value has too many parts. It must follow the pattern: 'Bearer xx.yy.zz' where xx.yy.zz is a valid JWT token.`,
      );
    }

    const token = parts[1];
    console.log(token);
    return token;
  }
}
