import {
  AuthenticationComponent,
  registerAuthenticationStrategy,
} from '@loopback/authentication';
import {BootMixin} from '@loopback/boot';
import {ApplicationConfig} from '@loopback/core';
import {RepositoryMixin} from '@loopback/repository';
import {RestApplication} from '@loopback/rest';
import {
  RestExplorerBindings,
  RestExplorerComponent,
} from '@loopback/rest-explorer';
import {ServiceMixin} from '@loopback/service-proxy';
import path from 'path';
import {JWTAuthenticationStrategy} from './jwt-strategy';
import {TokenServiceBindings, TokenServiceInstant} from './keys';
import {MySequence} from './sequence';
import {JwtService} from './services/jwt.service';

export {ApplicationConfig};

export class CrudApplication extends BootMixin(
  ServiceMixin(RepositoryMixin(RestApplication)),
) {
  constructor(options: ApplicationConfig = {}) {
    super(options);

    // Setup binding;
    this.setupBindings();

    // Setup Auth Strat
    this.component(AuthenticationComponent);
    registerAuthenticationStrategy(this, JWTAuthenticationStrategy);

    // Set up the custom sequence
    this.sequence(MySequence);

    // Set up default home page
    this.static('/', path.join(__dirname, '../public'));

    // Customize @loopback/rest-explorer configuration here
    this.configure(RestExplorerBindings.COMPONENT).to({
      path: '/explorer',
    });
    this.component(RestExplorerComponent);

    this.projectRoot = __dirname;
    // Customize @loopback/boot Booter Conventions here
    this.bootOptions = {
      controllers: {
        // Customize ControllerBooter Conventions here
        dirs: ['controllers'],
        extensions: ['.controller.js'],
        nested: true,
      },
    };
  }

  setupBindings(): void {
    this.bind(TokenServiceBindings.TOKEN_EXPIRE).to(
      TokenServiceInstant.TOKEN_EXPIRE_VALUE,
    );
    this.bind(TokenServiceBindings.TOKEN_SECRET).to(
      TokenServiceInstant.TOKEN_SECRET_VALUE,
    );
    this.bind(TokenServiceBindings.TOKEN_SERVICE).toClass(JwtService);
  }
}
